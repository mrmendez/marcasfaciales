
#USAGE: python Videofacial_68_Landmark.py

import dlib,cv2
import numpy as np
from facePoints import facePoints

captura = cv2.VideoCapture(2)
font = cv2.FONT_HERSHEY_SIMPLEX

def writeFaceLandmarksToLocalFile(faceLandmarks, fileName):
  with open(fileName, 'w') as f:
    for p in faceLandmarks.parts():
      f.write("%s %s\n" %(int(p.x),int(p.y)))

  f.close()

# location of the model (path of the model).
Model_PATH = "shape_predictor_68_face_landmarks.dat"


# now from the dlib we are extracting the method get_frontal_face_detector()
# and assign that object result to frontalFaceDetector to detect face from the image with 
# the help of the 68_face_landmarks.dat model
frontalFaceDetector = dlib.get_frontal_face_detector()

# Now the dlip shape_predictor class will take model and with the help of that, it will show 
faceLandmarkDetector = dlib.shape_predictor(Model_PATH)

while ( captura.isOpened() ) :

  # Ahora estamos leyendo un cuadro de imagen de video usando openCV 
  ret, img = captura.read();
  imageRGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
  
  # Now this line will try to detect all faces in an image either 1 or 2 or more faces
  allFaces = frontalFaceDetector(imageRGB, 0)
  
  print("List of all faces detected: ",len(allFaces))
  
  # Below loop we will use to detect all faces one by one and apply landmarks on them
  
  for k in range(0, len(allFaces)):
    # dlib rectangle class will detecting face so that landmark can apply inside of that area
    faceRectangleDlib = dlib.rectangle(int(allFaces[k].left()),int(allFaces[k].top()),
        int(allFaces[k].right()),int(allFaces[k].bottom()))

    # Now we are running loop on every detected face and putting landmark on that with the help of faceLandmarkDetector
    detectedLandmarks = faceLandmarkDetector(imageRGB, faceRectangleDlib)
  
    # Now finally we drawing landmarks on face
    facePoints(img, detectedLandmarks)
    if ret == True:
      cv2.imshow("Face landmark result", img)
  if cv2.waitKey(1) & 0xFF == ord('s'):
    break;
# Pause screen to wait key from user to see result
captura.release()
cv2.putText(img, 'Presiona', (10,70), font, 3, (0, 255, 0), 2, cv2.LINE_AA)
cv2.putText(img, 'cualquier', (10,150), font, 3, (0, 255, 0), 2, cv2.LINE_AA)
cv2.putText(img, 'tecla para', (10,230), font, 3, (0, 255, 0), 2, cv2.LINE_AA)
cv2.putText(img, 'salir', (10,310), font, 3, (0, 255, 0), 2, cv2.LINE_AA)
cv2.imshow("Face landmark result", img)
cv2.waitKey(0)
cv2.destroyAllWindows()
